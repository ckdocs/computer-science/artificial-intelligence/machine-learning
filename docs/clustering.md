# Clustering Analysis

Like goes with likes, find groups of **Data Objects** that are similar to each other

## Dissimilarity

Is a matrix that show the similarity of every two **DataObject** in **DataSet**

$$
\begin{aligned}
    & d(X,Y) \in [0,1] = \texttt{Dissimilarity}
    \\
    & sim(X,Y) \in [0,1] = 1 - d(X,Y) = \texttt{Similarity}
    \\
    \\
    & d(DataSet) =
    \begin{bmatrix}
        d(X_1,X_1) & d(X_1,X_2) & \dots & d(X_1,X_n) \\
        d(X_2,X_1) & d(X_2,X_2) & \dots & d(X_2,X_n) \\
        \vdots & \vdots & \vdots & \vdots \\
        d(X_n,X_1) & d(X_n,X_2) & \dots & d(X_n,X_n) \\
    \end{bmatrix}
\end{aligned}
$$

![Dissimilarity](assets/clustering_dissimilarity.png)

There are many ways to find **Dissimilarity** of two **DataObject** or **Vector**

1. **Absolute-value norm**
2. **Euclidean norm**
3. **Manhattan norm**
4. **p-norm**
5. **Maximum norm**
6. **Zero norm**
7. etc

<!--prettier-ignore-->
!!! tip "Qualitative vs Quantitative"
    There are multiple data types, and for each one we have multiple ways to find **Distance** or **Norm**

    1. Qualitative
       1. Nominal: 0,1
       2. Ordinal: 0,1,2,...
    2. Quantitative
       1. Norms

---

## k-Means

## k-Medoids

## kNN Clustering

## Hierachical Clustering

For example in **Spotify** we have multiple genres that have subgenres
