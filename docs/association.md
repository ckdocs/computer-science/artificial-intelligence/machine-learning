# Association Analysis

1. Find Frequent ItemSets
2. Generate Strong Rules

## Frequent ItemSet

Calculate **support** for combination of items

## Rule Generation

If/Then statements, calculate **confidence** or **conditional probability**

<!-- Add Confidence (Conditional Probability) to probability -->

## Apriori

Breath First Search (BFS)

## ECLAT (Equivalent Class Transformation)

Depth First Search (DFS)

## FP-Growth (Frequent Pattern Growth)

## RElim (Recursive Elimination)

## SaM (Split and Merge)

## JIM (Jaccard ItemSet Mining)
