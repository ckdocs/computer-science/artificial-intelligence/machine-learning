# Outlier Analysis

## IQR

Every data in out of this range is **Outlier**

$$
\begin{aligned}
    & [Q_1 - 1.5 IQR, Q_3 + 1.5 IQR]
\end{aligned}
$$

![IQR](assets/outlier_iqr.png)

## Delete

## Transform

## Robust

Use Decision Tree
