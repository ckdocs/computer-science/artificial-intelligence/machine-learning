# Intro

Machine Learning is a field of computer science, used to give computers ability to learn without hard coding

<!--prettier-ignore-->
!!! tip "Arthur Samuel (1959)"
    Machine Learning: Field of study that gives computers the ability to learn without being explicitly programmed.

    ![Arthur Samuel](assets/arthur_samuel.jpg)

    He created IBM checkers game by learning it, 10,000 player games

<!--prettier-ignore-->
!!! tip "Tom Mitchell (1998)"
    Machine Learning: A computer program is said to learn from experience `E` with resect to some task `T` and some performance measure `P`, if it's performance on `T`, as measured by `P`, imporves with experence `E`

    ![Tom Mitchell](assets/tom_mitchell.jpg)

---

## Algorithms

Machine Learning consists of many different **Algorithms**, these algorithms categorizes into 3 categories:

1. **Supervised Learning**
2. **Unsupervised Learning**
3. **Reinforcement Learning**

![Machine Learning Algorithms](assets/machine_learning_algorithms.png)

---
