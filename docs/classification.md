# Classification Analysis

Is a type of prediction used for **Qualitative DataAttributes**, based on **DataSet** values

Used to predict a **qualitative** attribute of a **Data Object**

<!--prettier-ignore-->
!!! tip "Options"
    There are many options in each machine learning algorithms to configure

    1. Thereshold
    2. Accuracy
    3. etc

## Binary Classification vs Multi-Class Classification

## k-Nearest Neighbors (KNN)

## Naive Bayes

## Decision Tree

## Rule Based

## Random Forest

## Support Vector Machine

## Artifical Neural Networks (ANN)

## Logistic Regression

## Bayesian Belief Network (BBN)
